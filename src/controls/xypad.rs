use crate::{Control, Commandable, AppState};
use crate::layout::{Point, Rect};
use crate::outputs::midi;
use super::{ControlError, ControlInfo, KeyDirection, InputHandled};

use log::*;

const MARK_BRIGHTNESS: u8 = 6;
const TOUCH_BRIGHTNESS: u8 = 15;

/// `xypad`: An X/Y pad that controls 2 CC values.
pub struct XYPad {
    pos: Point,
    cc1: u8,
    cc2: u8,
}

fn scale_for_cc(pos: i32, max: i32) -> u8 {
    let value = (127.0 / (max as f32 / pos as f32)).floor() as u8;
    debug!("scaled {} to {}", pos, value);
    return value;
}

impl Control for XYPad {
    fn show(self: &mut Self, state: &mut AppState, rect: Rect) {
        let draw = state.get_draw();
        self.pos = Point { x: rect.w / 2, y: rect.w / 2 };
        draw.set(self.pos.x + rect.x, self.pos.y + rect.y, MARK_BRIGHTNESS);
    }

    fn handle_key(self: &mut Self, state: &mut AppState, rect: Rect, touch: Point,
                  direction: &KeyDirection) -> InputHandled {
        debug!("Received {:?} at relative {}", direction, touch);
        let draw = state.get_draw();
        let play = state.get_play();
        match direction {
            KeyDirection::Down => {
                draw.set(self.pos.x + rect.x, self.pos.y + rect.y, 0);
                self.pos = Point { x: touch.x - rect.x, y: touch.y - rect.y };
                draw.set(touch.x, touch.y, TOUCH_BRIGHTNESS);

                play.post_command(midi::cc(self.cc1, scale_for_cc(self.pos.x, rect.w)))
                    .unwrap();
                play.post_command(midi::cc(self.cc2, scale_for_cc(self.pos.y, rect.h)))
                    .unwrap();
            },
            KeyDirection::Up => {
                if touch.x == self.pos.x + rect.x && touch.y == self.pos.y + rect.y {
                    draw.set(touch.x, touch.y, MARK_BRIGHTNESS);
                }
            },
        }
        return true;
    }
}

impl XYPad {
    pub fn new(cc1: u8, cc2: u8) -> Self {
        Self {
            pos: Point { x: 0, y: 0 },
            cc1, cc2
        }
    }

    pub fn new_from_info(info: &ControlInfo) -> Result<Self, ControlError> {
        let cc1 = serde_yaml::from_str(info.attrs.get("cc1").unwrap_or(&"0".to_string()))?;
        let cc2 = serde_yaml::from_str(info.attrs.get("cc2").unwrap_or(&"0".to_string()))?;
        Ok(XYPad::new(cc1, cc2))
    }
}
