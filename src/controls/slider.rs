use crate::{Command, Control, AppState, Draw};
use crate::commands;
use crate::layout::{Orientation, Point, Rect};
use super::{ControlError, ControlInfo, KeyDirection, InputHandled};

use log::*;
use rosc::OscType;

const LED_BACKGROUND: u8 = 0;
const LED_MARK: u8 = 6;
const LED_TOUCH: u8 = 15;

type Value = u8;

/// `slider.horizontal` and `slider.vertical`: controls a single value.
///
/// ## Commands
///
///  * `/value/set` ([u8]): update the slider position.
pub struct Slider {
    orientation: Orientation,
    touch: Option<Point>,

    target: String,
    min: Value,
    max: Value,
    value: Value,
}

impl Control for Slider {
    fn show(self: &mut Self, state: &mut AppState, rect: Rect) {
        self.redraw(state.get_draw(), rect);
    }

    fn handle_key(self: &mut Self, state: &mut AppState, rect: Rect, touch: Point,
                  direction: &KeyDirection) -> InputHandled {
        debug!("Received {:?} at relative {}", direction, touch);
        match direction {
            KeyDirection::Down => {
                self.touch = Some(touch);
                self.value = match self.orientation {
                    Orientation::HORIZONTAL => {
                        let new_pos = touch.x - rect.x;
                        self.position_to_value(new_pos, rect.w - 1)
                    },
                    Orientation::VERTICAL => {
                        let new_pos = touch.y - rect.y;
                        self.position_to_value(new_pos, rect.h - 1)
                    }
                };

                match commands::parse_command(&self.target) {
                    Ok(mut command) => {
                        command.args.push(OscType::Double(self.value_to_f64(self.value)));
                        state.post_command(command)
                            .unwrap();
                    },
                    Err(error) => warn!("{}", error),
                }
            },
            KeyDirection::Up => {
                self.touch = None;
            },
        }
        self.redraw(state.get_draw(), rect);
        return true;
    }

    fn process_command(self: &mut Self, command: Command, draw: &Draw, rect: Rect) {
        match command.addr.as_str() {
            "/value/set" => {
                if command.args.len() == 1 {
                    let value = commands::expect_arg_f64(&command.args[0]).unwrap();
                    self.value = self.f64_to_value(value);
                    self.redraw(draw, rect);
                } else {
                    warn!("Method {} expects 1 arg, got {}", command.addr, command.args.len());
                }
            },
            _ => warn!("Invalid method for slider: {}", command.addr)
        }
    }
}

impl Slider {
    pub fn new(orientation: Orientation, target: &str, min: Value, max: Value, value: Value) -> Self {
        Self {
            orientation,
            target: target.to_string(), min, max, value,
            touch: None,
        }
    }

    pub fn new_from_info(info: &ControlInfo) -> Result<Self, ControlError> {
        if ! info.attrs.contains_key("target") {
            return Err(ControlError { message: "Slider must have 'target' attribute set.".to_string() });
        }

        let orientation: Orientation = serde_yaml::from_str(
            info.attrs.get("orientation").unwrap_or(&"HORIZONTAL".to_string())
        )?;

        let target = info.attrs.get("target").unwrap();

        let min: Value = serde_yaml::from_str(
            info.attrs.get("min").unwrap_or(&"0".to_string())
        )?;
        let max: Value = serde_yaml::from_str(
            info.attrs.get("max").unwrap_or(&"127".to_string())
        )?;
        let value: Value = serde_yaml::from_str(
            info.attrs.get("value").unwrap_or(&max.to_string())
        )?;

        Ok(Slider::new(orientation, target, min, max, value))
    }

    fn redraw(self: &mut Self, draw: &Draw, rect: Rect) {
        draw.rect(rect.x, rect.y, rect.x + rect.w, rect.y + rect.h, LED_BACKGROUND);

        self.draw_marker(draw, rect);
        self.draw_touch(draw, rect);
    }

    fn draw_marker(self: &mut Self, draw: &Draw, rect: Rect) {
        let pos = self.value_to_position(self.value, rect);
        match self.orientation {
            Orientation::HORIZONTAL => {
                draw.vline(pos + rect.x, rect.y, rect.y + rect.h, LED_MARK);
            },
            Orientation::VERTICAL => {
                draw.hline(rect.x, rect.x + rect.w, pos + rect.y, LED_MARK);
            }
        }
    }

    fn draw_touch(self: &mut Self, draw: &Draw, rect: Rect) {
        if let Some(point) = self.touch {
            debug!("Drawing touch at {:?}", point);
            match self.orientation {
                Orientation::HORIZONTAL => {
                    draw.vline(point.x, rect.y, rect.y + rect.h, LED_TOUCH);
                },
                Orientation::VERTICAL => {
                    draw.hline(rect.x, rect.x + rect.w, point.y, LED_TOUCH);
                }
            }
        }
    }

    fn position_to_value(self: &Self, pos: i32, max_pos: i32) -> Value {
        let pos_inverted = max_pos - pos ;
        let range = self.max - self.min;
        let value = (range as f32 / (max_pos as f32 / pos_inverted as f32)).floor() as Value + self.min;
        debug!("scaled position {} to {}", pos, value);
        return value;
    }

    fn value_to_position(self: &Self, value: Value, rect: Rect) -> i32 {
        let value_inverted = self.max - value;
        let range = match self.orientation {
            Orientation::HORIZONTAL => rect.w - 1,
            Orientation::VERTICAL => rect.h - 1,
        };
        let pos = ((value_inverted as f32 / self.max as f32) * range as f32).floor() as i32;
        debug!("scaled value {} to {}", value, pos);
        return pos;
    }

    fn f64_to_value(self: &Self, value_f64: f64) -> u8 {
        let range = f64::from(self.max - self.min);
        return self.min + (value_f64 * range).floor() as u8;
    }

    fn value_to_f64(self: &Self, value: u8) -> f64 {
        let range = f64::from(self.max - self.min);
        return (value - self.min) as f64 / range;
    }
}
