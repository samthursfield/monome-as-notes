// msgs to send:
//
//
// /track/?/exists
// /track/?/selected
// /track/?/selected
// /track/bank/+
// /track/bank/page/+
// /track/1/select -- double to expand
// /device/+
// /device/sibling/1-8/selected
// /device/primary

pub struct ChannelSelect {
    selected_bank: u32,
    selected_track: u32,
    selected_device: u32,
}

impl Control for Toggle {
    fn select_track_bank(self: &mut Self, bank: u32) {
        if bank < self.selected_bank {
            let steps = self.selected_bank - bank;
            for i in 0..steps {
                msg_buf = encoder::encode(&OscPacket::Message(OscMessage {
                    addr: "/track/bank/+".to_string(),
                })).unwrap();
                sock.send_to(&msg_buf, to_addr).unwrap();
            }
        } else if bank > self.selected_bank {
            let steps = bank - self.selected_bank;
            for i in 0..steps {
                msg_buf = encoder::encode(&OscPacket::Message(OscMessage {
                    addr: "/track/bank/-".to_string(),
                })).unwrap();
                sock.send_to(&msg_buf, to_addr).unwrap();
            }
        }
        self.selected_bank = bank;
    }

    fn select_track(self: &mut Self, track: u32) {
        let bank = track / 8;
        let track_in_bank = track % 8;
        self.select_track_bank(bank);
        msg_buf = encoder::encode(&OscPacket::Message(OscMessage {
            addr: "/track/{track_in_bank}/select".to_string(),
        })).unwrap();
        sock.send_to(&msg_buf, to_addr).unwrap();
    }

    fn handle_key(self: &mut Self, _state: &mut AppState, rect: Rect, touch: Point,
                  direction: &KeyDirection) -> InputHandled {
        debug!("Received {:?} at point {})", direction, touch);
        if touch.x == 0 + rect.x {
            let track = touch.y - rect.y;
            if direction == KeyDirection::Down => {
                self.select_track(track);
            }
        }

        return true;
    }
}
