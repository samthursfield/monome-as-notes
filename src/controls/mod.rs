//! Individual controls that can be used in a [layout](crate::layout).
//!
//! Each control is a struct that implements the [Control] trait. See
//! the structs listed below for the available control types.

mod bitwig_track_select;
mod keyboard;
mod slider;
mod snake_view;
mod toggle;
mod xypad;

use super::layout::{Point, Rect};
use crate::AppState;
use crate::commands::{Command};
use crate::Draw;

pub use monome::KeyDirection;
use serde::Deserialize;

pub use bitwig_track_select::BitwigTrackSelect;
pub use keyboard::{LinearKeyboard, PressureKeyboard};
pub use slider::Slider;
pub use snake_view::SnakeView;
pub use toggle::Toggle;
pub use xypad::XYPad;

use std::sync::{Arc, Mutex};
use std::collections::HashMap;

/// Error type used by [Control] trait.
#[derive(Debug)]
pub struct ControlError {
    pub message: String,
}

impl From<serde_yaml::Error> for ControlError {
    fn from(error: serde_yaml::Error) -> Self {
        ControlError {
            message: error.to_string(),
        }
    }
}

pub type InputHandled = bool;

pub type ControlAttrs = HashMap<String, String>;

fn default_attrs() -> ControlAttrs { ControlAttrs::new() }

/// Serializable info about a control, used to load [layouts](crate::layout) from YAML.
#[derive(Clone, Debug, Deserialize)]
pub struct ControlInfo {
    pub name: String,
    #[serde(rename = "type")]
    pub type_: String,
    pub pos: [i8;2],
    pub size: [i8;2],
    #[serde(default = "default_attrs")]
    pub attrs: ControlAttrs,
}

/// Trait for controls to process grid & OSC commands, and update grid state.
///
/// See [crate::controls] for overview.
// FIXME: should be called HandleInput rather than Control ... ??
// Following file:///usr/share/doc/rust/html/book/ch17-02-trait-objects.html
pub trait Control {
    fn show(self: &mut Self, _leds: &mut AppState, _rect: Rect) {
    }

    fn handle_key(self: &mut Self, state: &mut AppState, rect: Rect, touch: Point,
                  direction: &KeyDirection) -> InputHandled;

    fn process_command(self: &mut Self, _command: Command, _draw: &Draw, _rect: Rect) {
    }

    fn frame(self: &mut Self, _state: &mut AppState, _rect: Rect) {
    }
}

macro_rules! control {
    ( $t:ty, $info:expr ) => {
        match <$t>::new_from_info($info) {
            Ok(c) => Ok(
                Arc::new(
                    Mutex::new(
                        c
                    )
                )
            ),
            Err(e) => Err(e),
        }
    }
}

/// Generate a Control from serialized data. Used by [crate::layout::load_layout_file].
pub fn new_from_info(info: &ControlInfo) -> Result<Arc<Mutex<dyn Control>>, ControlError> {
    match info.type_.as_str() {
        "bitwig-track-select" => control!(BitwigTrackSelect, &info),
        "linear-keyboard" => control!(LinearKeyboard, &info),
        "pressure-keyboard" => control!(PressureKeyboard, &info),
        "slider" => control!(Slider, &info),
        "slider.horizontal" => {
            let mut info = info.clone();
            info.attrs.insert("orientation".to_string(), "HORIZONTAL".to_string());
            control!(Slider, &info)
        },
        "slider.vertical" => {
            let mut info = info.clone();
            info.attrs.insert("orientation".to_string(), "VERTICAL".to_string());
            control!(Slider, &info)
        },
        "snake-view" => control!(SnakeView, &info),
        "toggle" => control!(Toggle, &info),
        "xypad" => control!(XYPad, &info),
        _ => Err(ControlError {
            message: format!("No control with type '{}'", info.type_).to_string()
        })
    }
}
