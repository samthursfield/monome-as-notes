use crate::{AppState, Commandable, Control};
use crate::draw::{LedBrightness};
use crate::layout::{Point, Rect};
use crate::outputs::midi;
use crate::units::{Pitch, PITCH_MAX};
use super::{ControlError, ControlInfo, KeyDirection, InputHandled};

use log::*;

use std::collections::HashMap;

fn point_to_pitch(point: Point, rect: Rect) -> Pitch {
    let stride: i32 = rect.w;
    let inverse_y = (rect.h) - (point.y - rect.y + 1);
    let pitch: u8 = point.x as u8 - rect.x as u8 + inverse_y as u8 * stride as u8;

    return pitch;
}

fn pitch_to_point(relative_pitch: Pitch, rect: Rect) -> Point {
    let x = i32::from(relative_pitch) % rect.w;
    let y = (rect.h - 1) - i32::from(relative_pitch) / (rect.w + 1);
    return Point {x, y};
}

// Returns the default brightness for a pitch.
fn pitch_marking(pitch: Pitch) -> u8 {
    let mut brightness: u8 = 0;
    if pitch % 12 == 0 {
        // It's a C
        brightness = 4;
    } else if pitch % 12 == 7 {
        // It's a G
        brightness = 2;
    }
    return brightness;
}

/// `linear-keyboard`: A horizontal pitch line.
///
/// When this is more than 1 row high, the line wraps around. The lowest pitch is at the
/// bottom-left corner and the highest pitch at the top-right.
pub struct LinearKeyboard {
    lowest_pitch: Pitch,
    light_markings: bool,
    channel: u8,
}

impl Control for LinearKeyboard {
    fn show(self: &mut Self, state: &mut AppState, rect: Rect) {
        let draw = state.get_draw();
        if self.light_markings {
            let highest_pitch = self.lowest_pitch + (rect.w as u8 * rect.h as u8);
            for pitch in self.lowest_pitch..highest_pitch {
                let brightness = pitch_marking(pitch);
                if brightness > 0 {
                    let point = pitch_to_point(pitch - self.lowest_pitch, rect);
                    draw.set(point.x, point.y, brightness);
                }
            }
        }
    }

    fn handle_key(self: &mut Self, state: &mut AppState, rect: Rect, touch: Point, direction: &KeyDirection) -> InputHandled {
        debug!("Received {:?} at {}", direction, touch);
        let draw = state.get_draw();
        let play = state.get_play();

        let pitch = (point_to_pitch(touch, rect) + self.lowest_pitch)
            .clamp(0, PITCH_MAX);
        match direction {
            KeyDirection::Down => {
                play.post_command(midi::note_on(self.channel, pitch, 0x64))
                    .unwrap();
                draw.set(touch.x, touch.y, 15);
            }
            KeyDirection::Up => {
                play.post_command(midi::note_off(self.channel, pitch))
                    .unwrap();
                draw.set(touch.x, touch.y, pitch_marking(pitch));
            }
        }

        return true;
    }
}

impl LinearKeyboard {
    pub fn new(lowest_pitch: Pitch, light_markings: bool, channel: u8) -> Self {
        LinearKeyboard { lowest_pitch, light_markings, channel }
    }

    pub fn new_from_info(info: &ControlInfo) -> Result<Self, ControlError> {
        let light_markings = serde_yaml::from_str(
            info.attrs.get("light-markings").unwrap_or(&"true".to_string())
        )?;
        let lowest_pitch = serde_yaml::from_str(
            info.attrs.get("lowest-pitch").unwrap_or(&"36".to_string())
        )?;
        let channel = serde_yaml::from_str(
            info.attrs.get("midi-channel").unwrap_or(&"1".to_string())
        )?;
        return Ok(LinearKeyboard::new(lowest_pitch, light_markings, channel));
    }
}

/// `pressure-keyboard`: a block where the X axis is a pitch line, and the Y access controls aftertouch
/// pressure per note.
pub struct PressureKeyboard {
    lowest_pitch: Pitch,
    light_markings: bool,
    channel: u8,
    active_touches: HashMap<Pitch, i32>,
}

impl Control for PressureKeyboard {
    fn show(self: &mut Self, state: &mut AppState, rect: Rect) {
        let draw = state.get_draw();
        if self.light_markings {
            for x in 0..rect.w {
                for y in 0..rect.h {
                    let brightness = self.point_marking(Point { x, y}, rect);
                    if brightness > 0 {
                        draw.set(x, y, brightness);
                    }
                }
            }
        }
    }

    fn handle_key(self: &mut Self, state: &mut AppState, rect: Rect, touch: Point, direction: &KeyDirection) -> InputHandled {
        debug!("Received {:?} at {}", direction, touch);
        let draw = state.get_draw();
        let play = state.get_play();

        let pitch: Pitch = ((touch.x - rect.x) as Pitch + self.lowest_pitch)
            .clamp(0, PITCH_MAX);
        let inverted_y: f32 = (rect.h - 1 - (touch.y - rect.y)) as f32;

        let pressure: u8 = (inverted_y * 127.0 / (rect.h as f32 - 1.0)).round() as u8;
        debug!("pressure: {} = {} / {} * 128", pressure, rect.h, inverted_y);

        match direction {
            KeyDirection::Down => {
                let touch_count = *self.active_touches.get(&pitch).unwrap_or(&0);
                if touch_count == 0 {
                    play.post_command(midi::note_on(self.channel, pitch, 0x64))
                        .unwrap();
                }
                play.post_command(midi::pressure(pitch, pressure))
                    .unwrap();
                draw.set(touch.x, touch.y, 15);
                self.active_touches.insert(pitch, touch_count + 1);
            }
            KeyDirection::Up => {
                let touch_count = *self.active_touches.get(&pitch).unwrap_or(&0);
                if touch_count == 1 {
                    play.post_command(midi::note_off(self.channel, pitch))
                        .unwrap();
                }
                draw.set(touch.x, touch.y, self.point_marking(touch, rect));
                self.active_touches.insert(pitch, touch_count - 1);
            }
        }

        return true;
    }

}

impl PressureKeyboard {
    pub fn new(lowest_pitch: Pitch, light_markings: bool, channel: u8) -> Self {
        Self {
            lowest_pitch,
            light_markings,
            channel,
            active_touches: HashMap::new(),
        }
    }

    pub fn new_from_info(info: &ControlInfo) -> Result<Self, ControlError> {
        let light_markings = serde_yaml::from_str(
            info.attrs.get("light_markings").unwrap_or(&"true".to_string())
        )?;
        let lowest_pitch = serde_yaml::from_str(
            info.attrs.get("lowest_pitch").unwrap_or(&"36".to_string())
        )?;
        let channel = serde_yaml::from_str(
            info.attrs.get("midi-channel").unwrap_or(&"1".to_string())
        )?;
        return Ok(PressureKeyboard::new(lowest_pitch, light_markings, channel));
    }

    fn point_marking(self: &Self, point: Point, rect: Rect) -> LedBrightness {
        if point.y == rect.h - 1 {
            // Bottom row
            let pitch = point_to_pitch(point, rect);
            return pitch_marking(pitch);
        } else {
            let inverted_y = rect.h - 1 - point.y;
            return (inverted_y) as u8;
        }
    }
}
