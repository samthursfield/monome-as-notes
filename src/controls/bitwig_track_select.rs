// msgs to send:
//
//
// /track/?/exists
// /track/?/selected
// /track/?/selected
// /track/bank/+
// /track/bank/page/+
// /track/1/select -- double to expand
// /device/+
// /device/sibling/1-8/selected
// /device/primary

use crate::{AppState, Command};
use crate::controls::{Control, ControlError, ControlInfo, InputHandled, KeyDirection};
use crate::layout::{Point, Rect};

use log::*;
use rosc::{OscType};

pub struct BitwigTrackSelect {
    selected_track_bank: i32,
    selected_track: i32,
    selected_device: i32,
}

impl Control for BitwigTrackSelect {
    fn handle_key(self: &mut Self, state: &mut AppState, rect: Rect, touch: Point,
                  direction: &KeyDirection) -> InputHandled {
        debug!("Received {:?} at point {})", direction, touch);
        if touch.x == 0 + rect.x {
            let track = touch.y - rect.y;
            match *direction {
                KeyDirection::Down => self.select_track(state, track),
                _ => {},
            }
        }

        return true;
    }
}

impl BitwigTrackSelect {
    pub fn new() -> Self {
        BitwigTrackSelect {
            selected_device: 0,
            selected_track: 0,
            selected_track_bank: 0,
        }
    }

    pub fn new_from_info(info: &ControlInfo) -> Result<Self, ControlError> {
        return Ok(BitwigTrackSelect::new());
    }

    fn select_track_bank(self: &mut Self, state: &mut AppState, bank: i32) {
        if bank < self.selected_track_bank {
            let steps = self.selected_track_bank - bank;
            for i in 0..steps {
                let msg = Command {
                    addr: "/output/bitwig_osc/track/bank/+".to_string(), args: vec!(),
                };
                state.post_command(msg)
                    .unwrap();
            }
        } else if bank > self.selected_track_bank {
            let steps = bank - self.selected_track_bank;
            for i in 0..steps {
                let msg = Command {
                    addr: "/output/bitwig_osc/track/bank/-".to_string(), args: vec!(),
                };
                state.post_command(msg)
                    .unwrap();
            }
        }
        self.selected_track_bank = bank;
    }

    fn select_track(self: &mut Self, state: &mut AppState, track: i32) {
        debug!("Select track {}", track);
        let bank = track / 8;
        let track_in_bank = track % 8 + 1;
        self.select_track_bank(state, bank);

        let msg = Command {
            addr: format!("/output/bitwig_osc/track/{track_in_bank}/select").to_string(),
            args: vec!(),
        };
        state.post_command(msg)
            .unwrap();
    }

}
