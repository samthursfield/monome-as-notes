use crate::{AppState, Control};
use crate::draw::{Draw};
use crate::grid;
use crate::layout::{Point, Rect};
use super::{ControlError, ControlInfo, KeyDirection, InputHandled};

use log::*;
use rand::distributions::Uniform;
use rand::prelude::*;
use std::sync::{Arc, Mutex};
use tokio::sync::mpsc;
use tokio::time::{self, Duration};

const KEY_BRIGHTNESS: u8 = 3;
const HEAD_BRIGHTNESS: u8 = 14;
const BODY_BRIGHTNESS: u8 = 9;
const FOOD_MIN_BRIGHTNESS: u8 = 8;
const FOOD_MAX_BRIGHTNESS: u8 = 15;

const INITIAL_LENGTH: usize = 3;
const MAX_LENGTH: usize = 128;

#[derive(Debug)]
enum GameKey {
    Up, Down, Left, Right
}

struct SnakeGame {
    rx: mpsc::Receiver<GameKey>,

    head_ptr: usize,
    length: usize,
    body: [Point;MAX_LENGTH],

    dir_x: i32,
    dir_y: i32,

    food_pos: Option<Point>,
    food_brightness: i8,
    food_brightness_delta: i8,

    key_up: Point,
    key_down: Point,
    key_left: Point,
    key_right: Point,
}

fn point_wrapped_move(point: Point, x_delta: i32, y_delta: i32, max_x: i32, max_y: i32) -> Point {
    let mut new_point = point;
    if x_delta != 0 {
        new_point.x += x_delta;
        if new_point.x >= max_x {
            new_point.x = 0;
        }
        if new_point.x < 0 {
            new_point.x = max_x - 1;
        }
    }
    if y_delta != 0 {
        new_point.y += y_delta;
        if new_point.y >= max_y {
            new_point.y = 0;
        }
        if new_point.y < 0 {
            new_point.y = max_y - 1;
        }
    }
    return new_point;
}

impl SnakeGame {
    fn prev_body_ptr(self: &Self, ptr: usize) -> usize {
        if ptr == 0 {
            return MAX_LENGTH -1
        }
        return ptr - 1;
    }

    fn next_body_ptr(self: &Self, ptr: usize) -> usize {
        if ptr + 1 >= MAX_LENGTH {
            return 0;
        }
        return ptr + 1;
    }

    fn point_in_body(self: &Self, point: Point) -> bool {
        let mut ptr = self.head_ptr;
        for _i in 0..self.length {
            ptr = self.prev_body_ptr(ptr);
            if point == self.body[ptr] {
                return true;
            }
        }
        return false;
    }

    fn new_food_position(self: &mut Self, rect: Rect) -> Point {
        const MAX_ITERATIONS: i32 = 100;

        let x_dist = Uniform::from(0..rect.w);
        let y_dist = Uniform::from(0..rect.h);
        for _i in 0..MAX_ITERATIONS {
            let mut rng = rand::thread_rng();
            let point = Point { x: x_dist.sample(&mut rng) as i32, y: y_dist.sample(&mut rng) as i32 };
            if !self.point_in_body(point) {
                return point;
            }
        }

        panic!("Failed to place food!");
    }

    fn create_snake(self: &mut Self, draw: &Draw, rect: Rect) {
        self.length = INITIAL_LENGTH;
        let head_pos = Point { x: rect.w / 2, y: rect.h / 2 };
        let mut point = head_pos;
        for i in (0..self.length).rev()  {
            self.body[i] = point;
            if point == head_pos {
                draw.set(point.x + rect.x, point.y + rect.y, HEAD_BRIGHTNESS);
            } else {
                draw.set(point.x + rect.x, point.y + rect.y, BODY_BRIGHTNESS);
            }
            point = point_wrapped_move(point, -1, 0, grid::MAX_X, grid::MAX_Y);
        }
        self.head_ptr = self.length - 1;
    }

    fn create_food(self: &mut Self, draw: &Draw, rect: Rect) {
        let food_pos = self.new_food_position(rect);
        self.food_pos = Some(food_pos);
        draw.set(food_pos.x, food_pos.y, self.food_brightness as u8);
    }

    fn create_arrows(self: &mut Self, draw: &Draw, rect: Rect) {
        self.key_up = Point { x: rect.x + rect.w - 2, y: rect.y + rect.h - 2 };
        self.key_down = Point { x: rect.x + rect.w - 2, y: rect.y + rect.h - 1 };
        self.key_left = Point { x: rect.x + rect.w - 3, y: rect.y + rect.h - 1 };
        self.key_right = Point { x: rect.x + rect.w - 1, y: rect.y + rect.h - 1 };
        draw.set(self.key_up.x, self.key_up.y, KEY_BRIGHTNESS);
        draw.set(self.key_down.x, self.key_down.y, KEY_BRIGHTNESS);
        draw.set(self.key_left.x, self.key_left.y, KEY_BRIGHTNESS);
        draw.set(self.key_right.x, self.key_right.y, KEY_BRIGHTNESS);
    }

    /// Set an LED to the background colour.
    fn draw_board(self: &Self, draw: &Draw, point: Point, rect: Rect) {
        if point == self.key_up || point == self.key_down || point == self.key_left || point == self.key_right {
            // Key marker.
            draw.set(point.x + rect.x, point.y + rect.y, KEY_BRIGHTNESS);
        } else {
            // Empty space.
            draw.set(point.x + rect.x, point.y + rect.y, 0);
        }
    }

    fn run_game_frame(self: &mut Self, draw: &Draw, rect: Rect) {
        // Check for key presses.
        let mut new_dir_x: i32 = 0;
        let mut new_dir_y: i32 = 0;
        if let Ok(key) = self.rx.try_recv() {
            match key {
                GameKey::Up => {
                    new_dir_x = 0;
                    new_dir_y = -1;
                },
                GameKey::Down => {
                    new_dir_x = 0;
                    new_dir_y = 1;
                },
                GameKey::Left => {
                    new_dir_x = -1;
                    new_dir_y = 0;
                },
                GameKey::Right => {
                    new_dir_x = 1;
                    new_dir_y = 0;
                },
            }
            info!("Got new dir: {}, {}", new_dir_x, new_dir_y);
        }

        if new_dir_x != 0 || new_dir_y != 0 {
            let head_pos = self.body[self.head_ptr];
            let prev_head_pos = self.body[self.prev_body_ptr(self.head_ptr)];
            // Check for U-turns before moving and ignore those.
            let new_head_pos = point_wrapped_move(head_pos, new_dir_x, new_dir_y, grid::MAX_X, grid::MAX_Y);
            if new_head_pos != prev_head_pos {
                self.dir_x = new_dir_x;
                self.dir_y = new_dir_y;
            }
        };

        // Update the head
        let mut head_pos = self.body[self.head_ptr];
        draw.set(head_pos.x + rect.x, head_pos.y + rect.y, BODY_BRIGHTNESS);

        self.head_ptr = self.next_body_ptr(self.head_ptr);

        head_pos = point_wrapped_move(head_pos, self.dir_x, self.dir_y, rect.w, rect.h);
        self.body[self.head_ptr] = head_pos;
        draw.set(head_pos.x + rect.x, head_pos.y + rect.y, HEAD_BRIGHTNESS);

        // Check for death!
        let mut ptr = self.head_ptr;
        for _ in 1..self.length {
            ptr = self.prev_body_ptr(ptr);
            if head_pos == self.body[ptr] {
                panic!("DEATH!!!! Score: {}", self.length);
            }
        }

        match self.food_pos {
            Some(food_pos) => {
                // Check for nourishment!
                if food_pos == head_pos {
                    self.length += 1;

                    let food_pos = self.new_food_position(rect);
                    self.food_pos = Some(food_pos);
                }
            },
            None => {},
        }

        // Turn off the tail
        let mut tail_ptr: isize = self.head_ptr as isize - self.length as isize;
        if tail_ptr < 0 {
            tail_ptr = tail_ptr + (MAX_LENGTH as isize);
        }
        let tail: Point = self.body[tail_ptr as usize];

        self.draw_board(draw, tail, rect);
    }

    fn run_food_frame(self: &mut Self, draw: &Draw, _rect: Rect) {
        match self.food_pos {
            Some(food_pos) => {
                self.food_brightness += self.food_brightness_delta;
                if self.food_brightness <= FOOD_MIN_BRIGHTNESS as i8 {
                    self.food_brightness_delta = 1;
                } else if self.food_brightness >= FOOD_MAX_BRIGHTNESS as i8 {
                    self.food_brightness_delta = -1;
                }
                draw.set(food_pos.x, food_pos.y, self.food_brightness as u8);
            },
            None => {},
        }
    }
}

async fn snake_frame_loop(game_mutex: Arc<Mutex<SnakeGame>>, draw: Draw, rect: Rect) {
    let mut frame_interval = time::interval(Duration::from_millis(1000 / 3));
    loop {
        frame_interval.tick().await;

        let game: &mut SnakeGame = &mut game_mutex.lock().unwrap();
        game.run_game_frame(&draw, rect);
    }
}

async fn food_frame_loop(game_mutex: Arc<Mutex<SnakeGame>>, draw: Draw, rect: Rect) {
    let mut frame_interval = time::interval(Duration::from_millis(1000 / 10));
    loop {
        frame_interval.tick().await;

        let game: &mut SnakeGame = &mut game_mutex.lock().unwrap();
        game.run_food_frame(&draw, rect);
    }
}

/// `snake-view`: An entertaining game of Snake!
pub struct SnakeView {
    game: Arc<Mutex<SnakeGame>>,
    tx: mpsc::Sender<GameKey>,
}

impl Control for SnakeView {
    fn show(self: &mut Self, state: &mut AppState, rect: Rect) {
        assert!(rect.h >= 4 && rect.w >= 4, "Minimum snake size is 6x6");

        {
            let game_mutex = self.game.clone();
            let draw: &Draw = state.get_draw();
            let mut game = game_mutex.lock().unwrap();

            game.create_snake(draw, rect);
            game.create_food(draw, rect);
            game.create_arrows(draw, rect);
        }

        let draw: Draw = (*state.get_draw()).clone();
        let game_mutex = self.game.clone();
        tokio::spawn(snake_frame_loop(game_mutex, draw, rect));

        let draw: Draw = (*state.get_draw()).clone();
        let game_mutex = self.game.clone();
        tokio::spawn(food_frame_loop(game_mutex, draw, rect));
    }

    fn handle_key(self: &mut Self, _state: &mut AppState, _rect: Rect, touch: Point,
                  direction: &KeyDirection) -> InputHandled {
        match direction {
            KeyDirection::Down => {
                let mut game_guard = self.game.lock().unwrap();
                let game = &mut game_guard;

                let mut key: Option<GameKey> = None;
                if game.key_up == touch {
                    key = Some(GameKey::Up);
                } else if game.key_down == touch {
                    key = Some(GameKey::Down);
                } else if game.key_left == touch {
                    key = Some(GameKey::Left);
                } else if game.key_right == touch {
                    key = Some(GameKey::Right);
                };

                if key.is_some() {
                    info!("Got new key: {:?}", key);
                    self.tx.try_send(key.unwrap()).ok();
                };
            },
            _ => {},
        }

        return true;
    }

}

impl SnakeView {
    pub fn new() -> Self {
        let (tx, rx) = mpsc::channel(2);

        let game = SnakeGame {
            rx: rx,
            head_ptr: 0,
            length: 0,
            body: [Point { x: 0, y: 0 };MAX_LENGTH],
            dir_x: 1,
            dir_y: 0,
            food_pos: None,
            food_brightness: FOOD_MAX_BRIGHTNESS as i8,
            food_brightness_delta: -1,
            key_up: Point { x: 0, y: 0 },
            key_down: Point { x: 0, y: 0 },
            key_left: Point { x: 0, y: 0 },
            key_right: Point { x: 0, y: 0 },
        };

        Self {
            game: Arc::new(Mutex::new(game)),
            tx: tx,
        }
    }

    pub fn new_from_info(_info: &ControlInfo) -> Result<Self, ControlError> {
        Ok(SnakeView::new())
    }

}
