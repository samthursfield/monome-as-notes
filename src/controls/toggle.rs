use crate::{Control, AppState};
use crate::layout::{Point, Rect};
use super::{ControlError, ControlInfo, KeyDirection, InputHandled};

use log::*;

/// `toggle`: An on/off latch.
pub struct Toggle {
    on: bool,
}

impl Control for Toggle {
    fn handle_key(self: &mut Self, _state: &mut AppState, _rect: Rect, touch: Point,
                  direction: &KeyDirection) -> InputHandled {
        debug!("Received {:?} at point {})", direction, touch);
        match direction {
            KeyDirection::Up => {
                self.on = !self.on;
                debug!("AppState now {}", self.on);
            },
            _ => {},
        }
        return true;
    }
}

impl Toggle {
    pub fn new() -> Toggle {
        Toggle {
            on: false,
        }
    }

    pub fn new_from_info(_info: &ControlInfo) -> Result<Toggle, ControlError> {
        Ok(Toggle::new())
    }
}
