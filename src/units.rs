//! Helper types for specific units.

/// Beats per minute.
pub type Bpm = f64;

pub type Pitch = u8;
pub const PITCH_MAX: Pitch = 127;
