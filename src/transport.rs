//! Global transport for setting tempo.

use crate::Command;
use crate::commands;
use crate::units::Bpm;

use std::sync::mpsc;

use anyhow::bail;

const DEFAULT_TEMPO: Bpm = 60.0;

#[derive(Copy, Clone, Debug)]
pub struct TransportState {
    pub tempo: Bpm,
}

pub struct Transport {
    state: TransportState,
    tx: mpsc::Sender<TransportState>,
    rx: mpsc::Receiver<TransportState>,
}

impl Transport {
    pub fn new() -> Self {
        let initial_state = TransportState {
            tempo: DEFAULT_TEMPO,
        };

        let (tx, rx) = mpsc::channel();

        Self {
            state: initial_state,
            tx, rx
        }
    }

    pub fn get_tempo(self: &Self) -> Bpm {
        self.state.tempo
    }

    pub fn process_commands(self: &mut Self) {
        for state in self.rx.try_iter() {
            self.state = state;
        }
    }

    pub fn get_delegate(self: &Self) -> TransportDelegate {
        TransportDelegate {
            tx: self.tx.clone()
        }
    }
}

pub struct TransportDelegate {
    tx: mpsc::Sender<TransportState>,
}

impl TransportDelegate {
    pub fn post_command(self: &Self, command: Command) -> anyhow::Result<()> {
        match command.addr.as_ref() {
            "/tempo" => {
                if command.args.len() == 1 {
                    let tempo = commands::expect_arg_f64(&command.args[0])?;
                    let state = TransportState {
                        tempo: tempo.clamp(0.1, 48000.0)
                    };
                    self.tx.send(state)?;
                    Ok(())
                } else {
                    bail!("Wrong number of arguments for 'tempo' method")
                }
            },
            _ => bail!("Tried to call invalid transport method: {}", command.addr),
        }
    }
}
