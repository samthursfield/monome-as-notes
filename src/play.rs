//! Public API to control output state.

use crate::{Command, Commandable};
use crate::commands;

use anyhow::bail;
use log::*;
use tokio::sync::mpsc;

#[derive(Clone, Debug)]
pub struct Play {
    sender: mpsc::UnboundedSender<Command>,
    output_name: String,
}

impl Play {
    pub fn new(sender: mpsc::UnboundedSender<Command>, output_name: &str) -> Self {
        Self { sender, output_name: output_name.to_string() }
    }

    fn send(self: &Self, command: Command) {
        debug!("Send {:?}", command);
        self.sender.send(command).unwrap();
    }
}

impl Commandable for Play {
    fn post_command(self: &Self, command: Command) -> Result<(), anyhow::Error> {
        debug!("Post command: {command:?}");
        let (namespace, subcommand) = commands::split_namespace(&command)?;
        if namespace == self.output_name {
            self.send(subcommand);
            Ok(())
        } else {
            bail!("Output does not exist: {}", namespace)
        }
    }
}
