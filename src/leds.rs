//! Controls the LED brightness of each grid square.
//!
//! Updates are stored in memory and sent to the grid when .send() is called.

use super::grid::{MAX_X,MAX_Y};
use super::draw::{Draw, DrawCommand, LedBrightness};

use log::*;

use monome::Monome;

use tokio::sync::mpsc;

pub struct Leds {
    led: [LedBrightness; (MAX_X * MAX_Y) as usize],
    dirty: bool,
    sender: mpsc::UnboundedSender<DrawCommand>,
    receiver: mpsc::UnboundedReceiver<DrawCommand>,
}

fn address(x: i32, y: i32) -> usize {
    (y * MAX_X + x) as usize
}

impl Leds {
    pub fn new() -> Leds {
        let (tx, rx) = mpsc::unbounded_channel();
        Leds {
            led: [0; (MAX_X * MAX_Y) as usize],
            dirty: true,
            sender: tx,
            receiver: rx,
        }
    }

    pub fn get_draw(self: &Self) -> Draw {
        Draw::new(self.sender.clone())
    }

    fn reset(self: &mut Self) {
        self.led = [0; (MAX_X * MAX_Y) as usize];
        self.dirty = true;
    }

    fn rect(self: &mut Self, x1: i32, y1: i32, x2: i32, y2: i32, brightness: LedBrightness) {
        for x in x1..x2 {
            for y in y1..y2 {
                self.led[address(x, y)] = brightness;
            }
        }
        self.dirty = true;
    }

    fn set(self: &mut Self, x: i32, y: i32, brightness: LedBrightness) {
        if x >= MAX_X {
            panic!("X out of bounds: {} > {}", x, MAX_X);
        }
        if y >= MAX_Y {
            panic!("Y out of bounds: {} > {}", y, MAX_Y);
        }
        self.led[address(x, y)] = brightness;
        self.dirty = true;
    }

    pub fn process_commands(self: &mut Self, monome_option: &mut Option<Monome>) {
        let mut batch: Vec<DrawCommand> = Vec::new();

        while let Ok(next) = self.receiver.try_recv() {
            batch.push(next);
        }

        for command in batch.iter() {
            debug!("LEDs command: {:?}", command);
            match command {
                DrawCommand::Reset => {
                    self.reset();
                },
                DrawCommand::Rect { x1, y1, x2, y2, brightness } => {
                    self.rect(*x1, *y1, *x2, *y2, *brightness);
                }
                DrawCommand::Set { x, y, brightness } => {
                    self.set(*x, *y, *brightness);
                }
            }
        }

        match monome_option {
            Some(monome) => {
                if self.dirty {
                    monome.set_all_intensity(&self.led);
                }
            },
            None => {},
        };

        self.dirty = false;
    }
}
