//! Public API to control LEDs state.

use crate::grid::{MAX_X,MAX_Y};

use tokio::sync::mpsc;

pub type LedBrightness = u8;

/// Control messages for the device LEDs.
#[derive(Debug)]
pub enum DrawCommand {
    /// Set all to 0.
    Reset,
    /// Draw a filled rectangle.
    Rect { x1: i32, y1: i32, x2: i32, y2: i32, brightness: LedBrightness },
    /// Set brightness of one LED.
    Set { x: i32, y: i32, brightness: LedBrightness },
}

#[derive(Clone)]
pub struct Draw {
    sender: mpsc::UnboundedSender<DrawCommand>
}

impl Draw {
    pub fn new(sender: mpsc::UnboundedSender<DrawCommand>) -> Self {
        Self { sender }
    }

    fn send(self: &Self, command: DrawCommand) {
        self.sender.send(command).unwrap();
    }

    pub fn reset(self: &Self) {
        self.send(DrawCommand::Reset);
    }

    pub fn hline(self: &Self, x1: i32, x2: i32, y: i32, brightness: LedBrightness) {
        self.send(DrawCommand::Rect { x1, y1: y, x2, y2: y + 1, brightness });
    }

    pub fn vline(self: &Self, x: i32, y1: i32, y2: i32, brightness: LedBrightness) {
        self.send(DrawCommand::Rect { x1: x, y1, x2: x + 1, y2, brightness });
    }

    pub fn rect(self: &Self, x1: i32, y1: i32, x2: i32, y2: i32, brightness: LedBrightness) {
        self.send(DrawCommand::Rect { x1, y1, x2, y2, brightness });
    }

    pub fn set(self: &Self, x: i32, y: i32, brightness: LedBrightness) {
        if x >= MAX_X {
            panic!("X out of bounds: {} > {}", x, MAX_X);
        }
        if y >= MAX_Y {
            panic!("Y out of bounds: {} > {}", y, MAX_Y);
        }
        self.send(DrawCommand::Set { x, y, brightness });
    }
}
