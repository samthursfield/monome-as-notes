//! A layout of controls over the grid.
//!
//! Controls are defined inside the [crate::controls] module. Each control can send a control
//! message when it is triggered or its value changes.
//!
//! ## Control message namespace.
//!
//! The internal message system is modelled after Open Sound Control.
//!
//! The following path namespaces are available:
//!
//!   * `/layout/{control_name}/...`: sends a message to the given [control][crate::controls]. Each control documents
//!     the messages it accepts.
//!   * `/midi/...`: sends a message to the [MIDI output](crate::output).
//!
//! ## YAML format
//!
//! You can define layouts using YAML.
//!
//! Here's a minimal example:
//!
//! ```yaml
//! name: Example layout
//! description: |
//!   This layout is an example. The description allows you to document your layout.
//!
//! ## List of controls in this layout.
//! controls:
//!  ## A list of control types is linked above.
//!  - type: slider.vertical
//!
//!    ## Name must be unique and a valid identifier. You might choose a name that indicates what it
//!    ## actually controls on the target instrument, e.g. 'modulation' or 'reverb'.
//!
//!    name: midi_cc_20
//!
//!    ## Location on the grid where the control appears. [0,0] is the top left corner.
//!    pos: [8, 0]
//!    size: [1, 8]
//!
//!    ## Control-specific attributes. In this case, we specify that the slider value
//!    ## sends a message to internal endpoint `/midi/cc/20` when pressed. See above
//!    ## for a list of message endpoints.
//!    attrs:
//!      target: /midi/cc/20
//! ```

use super::commands::{self, Command, Commandable};
use super::controls;
use super::controls::{Control, ControlInfo};
use super::Draw;
use super::grid::{MAX_X,MAX_Y};

use std::collections::HashMap;
use std::fmt;
use std::fmt::Display;
use std::fs::File;
use std::io::Read;
use std::sync::{Arc, Mutex};
use std::sync::mpsc;
use std::vec::Vec;

use log::*;
use serde::Deserialize;

type MapEntry = u32;
pub const MAX_CONTROLS: usize = 32;

#[derive(Deserialize)]
pub enum Orientation {
    HORIZONTAL,
    VERTICAL,
}

#[derive(Clone)]
#[derive(Copy)]
#[derive(Debug)]
#[derive(PartialEq)]
pub struct Point {
    pub x: i32,
    pub y: i32,
}

impl Display for Point {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

/// Represents the position of a control on the grid.
#[derive(Clone)]
#[derive(Copy)]
#[derive(Debug)]
pub struct Rect {
    pub x: i32,
    pub y: i32,
    pub w: i32,
    pub h: i32,
    pub z: i32,
}

/// Representation of a layout that can be serialized from YAML using
/// [serde_yaml::from_str()].
#[allow(dead_code)]
#[derive(Deserialize)]
pub struct LayoutInfo {
    name: String,
    description: Option<String>,
    controls: Vec<ControlInfo>,
}

/// A specific layout of controls on the Monome grid.
///
/// See the [layout module docs](crate::layout) for overview.
///
/// Commands supported:
///
///   * `/layout/{name}/*`: message passed to [control][Control] with {name}
///
/// Use [LayoutDelegate] to post commands.
///
pub struct Layout {
    pub map: [MapEntry; (MAX_X * MAX_Y) as usize],

    controls: Vec<Arc<Mutex<dyn Control>>>,
    rects: Vec<Rect>,
    control_names: HashMap<String, usize>,

    command_tx: mpsc::Sender<Command>,
    command_rx: mpsc::Receiver<Command>,
}

fn address(x: i32, y: i32) -> usize {
    (y * MAX_X + x) as usize
}

/// Result of a Layout::pick() operation. Returns the [control](Control) and its
/// location within the [layout](Layout).
pub type PickResult = (Arc<Mutex<dyn Control>>, Rect);

impl Layout {
    /// Create a blank layout.
    pub fn new_empty(_name: &str, _description: &str) -> Layout {
        let (command_tx, command_rx) = mpsc::channel::<Command>();
        Layout {
            map: [0; (MAX_X * MAX_Y) as usize],
            controls: Vec::new(),
            rects: Vec::new(),
            control_names: HashMap::new(),
            command_rx, command_tx,
        }
    }

    /// Constructor used by [load_layout_file].
    ///
    /// Creates layout based on a deserialized a [LayoutInfo] struct.
    pub fn new_from_info(info: &LayoutInfo) -> Layout {
        let mut layout = Self::new_empty(
            &info.name, info.description.as_ref().unwrap_or(&"".to_string())
        );

        for control_info in &info.controls {
            let rect = Rect {
                x: i32::from(control_info.pos[0]),
                y: i32::from(control_info.pos[1]),
                w: i32::from(control_info.size[0]),
                h: i32::from(control_info.size[1]),
                z: 0,
            };
            match controls::new_from_info(&control_info) {
                Ok(control_rc) => {
                    layout.add_control(&control_info.name, rect, control_rc)
                }
                Err(e) => {
                    panic!("{}", e.message)
                }
            }
        }

        layout
    }

    /// Add a new control to the list.
    ///
    /// # Panics
    ///
    /// If the layout has the maximum number of controls already. See MAX_CONTROLS.
    pub fn add_control(self: &mut Self, name: &str, rect: Rect, control: Arc<Mutex<dyn Control>>) {
        debug!("Add control at {:?}", rect);
        let idx = self.controls.len();

        assert!(
            idx < MAX_CONTROLS,
            "Layout has more than the maximum number of controls",
        );
        assert!(self.controls.len() == self.rects.len());

        self.controls.push(control);
        self.rects.push(rect);
        self.control_names.insert(name.to_string(), idx);

        let bitmask: MapEntry = 1 << idx;
        for x in rect.x..rect.x+rect.w {
            for y in rect.y..rect.y+rect.h {
                self.map[address(x, y)] |= bitmask;
            }
        }
    }

    /// Get specific control with the given name. [None] if not found.
    pub fn get_control_by_name (self: &Self, name: &str) -> Option<PickResult> {
        match self.control_names.get(name) {
            Some(i) => Some((self.controls[*i].clone(), self.rects[*i])),
            None => None,
        }
    }

    /// Get all controls.
    pub fn pick_all(self: &Self) -> Vec<PickResult> {
        return Vec::from_iter(self.controls.clone().into_iter().zip(self.rects.clone()));
    }

    /// Get the controls at position (x,y).
    ///
    /// Returns a list of tuples (Control, Rect).
    ///
    /// There may be multiple controls at one point. The list is sorted in depth order
    /// with the highest depth (z) value first.
    pub fn pick(self: &Self, x: i32, y: i32) -> Vec<PickResult> {
        let bitmask = self.map[address(x,y)];
        let mut result = Vec::<PickResult>::new();

        for i in 0..MAX_CONTROLS {
            if (bitmask & (1 << i)) != 0 {
                result.push((self.controls[i].clone(), self.rects[i]));
            }
        }

        // FIXME: the list is not actually sorted by Z order yet.
        // Multiple layered controls is an advanced feature that I don't use at the moment.
        return result;
    }

    /// Get the [LayoutDelegate] which you can use to send [commands](Command).
    pub fn get_delegate(self: &Self) -> LayoutDelegate {
        LayoutDelegate { command_tx: self.command_tx.clone() }
    }

    /// Called from the event loop to process the command queue.
    pub fn process_commands(self: &mut Self, draw: &Draw) {
        for command in self.command_rx.try_iter() {
            let (control_name, sub_command) = commands::split_namespace(&command).unwrap();
            match self.get_control_by_name(control_name) {
                Some((control, rect)) => {
                    control
                        .lock()
                        .unwrap()
                        .process_command(sub_command, draw, rect);
                },
                None => panic!("Invalid control {}", control_name),
            }
        }
    }
}

/// Delegate for posting commands to Layout.
pub struct LayoutDelegate {
    command_tx: mpsc::Sender<Command>,
}

impl Commandable for LayoutDelegate {
    fn post_command(self: &Self, command: Command) -> anyhow::Result<()> {
        // FIXME: validate name
        self.command_tx.send(command)?;
        Ok(())
    }
}

/// Load layout from the specified YAML file.
pub fn load_layout_file(path: &str) -> Result<Layout, anyhow::Error> {
    let mut file = File::open(path)?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;

    let layout_info: LayoutInfo = serde_yaml::from_str(&contents)?;
    let layout = Layout::new_from_info(&layout_info);
    return Ok(layout);
}
