//! Output that collects Play events, and records them internally so
//! tests can inspect them.

use crate::{Command, Play};
use super::Output;

use tokio::sync::mpsc;

pub struct TestOutput {
    sender: mpsc::UnboundedSender<Command>,
}

impl Output for TestOutput {
    fn get_play(self: &Self) -> Play {
        Play::new(self.sender.clone(), "midi")
    }

    fn process_commands(self: &mut Self) {}
}

impl TestOutput {
    /// This is used only in unit tests.
    #[allow(dead_code)]
    pub fn new() -> (Self, mpsc::UnboundedReceiver<Command>) {
        let (tx, rx) = mpsc::unbounded_channel();
        (TestOutput { sender: tx }, rx)
    }
}
