///! Output modules.

pub mod bitwig_osc;
pub mod midi;
pub mod test;

use crate::Play;
pub use midi::MidiOutput;
pub use test::TestOutput;

use anyhow::bail;
use std::collections::HashMap;

/// A method for sending musical events.
pub trait Output {
    fn get_play(self: &Self) -> Play;

    fn process_commands(self: &mut Self);
}

pub fn all_outputs() -> Vec<&'static str> {
    vec!("midi", "bitwig_osc")
}

pub fn create_output(type_name: &str, config: HashMap<&str, &str>) -> Result<Box<dyn Output>, anyhow::Error> {
    match type_name {
        "midi" => {
            let output = midi::create_midi_output(config)?;
            Ok(Box::new(output))
        },
        "bitwig_osc" => {
            let output = bitwig_osc::create_bitwig_osc_output(config)?;
            Ok(Box::new(output))
        },
        _ => bail!("Invalid output type name: {}", type_name),
    }
}
