//! Output for controlling Bitwig Studio via Open Sound Control messages.
//!
//! This output targets the Driven by Moss OSC interface for Bitwig Studio. See:
//! https://github.com/git-moss/DrivenByMoss-Documentation/blob/master/Generic-Tools-Protocols/Open-Sound-Control-(OSC).md

use crate::{Command, Output, Play};

use anyhow::{Context, bail};
use log::*;
use rosc::{OscMessage, OscPacket, encoder};
use tokio::sync::mpsc;

use std::collections::HashMap;
use std::net::{Ipv4Addr, SocketAddrV4, UdpSocket};


pub struct BitwigOscOutput {
    socket: UdpSocket,
    to_addr: SocketAddrV4,

    sender: mpsc::UnboundedSender<Command>,
    receiver: mpsc::UnboundedReceiver<Command>,
}

impl Output for BitwigOscOutput {
    fn get_play(self: &Self) -> Play {
        Play::new(self.sender.clone(), "bitwig_osc")
    }

    fn process_commands(self: &mut Self) {
        let mut batch: Vec<Command> = Vec::new();

        while let Ok(next) = self.receiver.try_recv() {
            batch.push(next);
        }

        for command in batch.iter() {
            debug!("Bitwig OSC command: {:?}", command);
            self.send(command.clone()).unwrap();
        }
    }
}

impl BitwigOscOutput {
    pub fn new(socket: UdpSocket, to_addr: SocketAddrV4) -> BitwigOscOutput {
        let (tx, rx) = mpsc::unbounded_channel();
        BitwigOscOutput { socket, to_addr, sender: tx, receiver: rx }
    }

    fn send(self: &mut Self, message: OscMessage) -> Result<(), anyhow::Error> {
        info!("Sending Bitwig OSC Message: {:?}", message);
        let packet = encoder::encode(&OscPacket::Message(message))?;
        self.socket.send_to(&packet, self.to_addr)?;
        Ok(())
    }
}

pub fn create_bitwig_osc_output(mut config: HashMap<&str, &str>) -> Result<BitwigOscOutput, anyhow::Error> {
    let mut osc_port: u16 = 8000;

    for (key, value) in config.drain() {
        match key {
            "port" => {
                osc_port = value.parse()
                    .with_context(|| format!("Couldn't parse `port` value: {}", value))?;
            }
            _ => {
                bail!("Unknown config key for Bitwig OSC output: {}", key)
            }
        }
    }

    let host_addr =SocketAddrV4::new(Ipv4Addr::new(127,0,0,1), 0);
    let socket = UdpSocket::bind(host_addr)?;
    let to_addr = SocketAddrV4::new(Ipv4Addr::new(0,0,0,0), osc_port);
    Ok(BitwigOscOutput::new(socket, to_addr))
}
