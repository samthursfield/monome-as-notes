mod test_output {
//! Output that collects Play events, and records them internally so
//! tests can inspect them.

use super::*;

use tokio::sync::mpsc;

pub struct TestOutput {
    sender: mpsc::UnboundedSender<PlayCommand>,
}

impl Output for TestOutput {
    fn get_play(self: &Self) -> Play {
        Play::new(self.sender.clone())
    }

    fn process_commands(self: &mut Self) {}
}

impl TestOutput {
    pub fn new() -> (Self, mpsc::UnboundedReceiver<PlayCommand>) {
        let (tx, rx) = mpsc::unbounded_channel();
        (TestOutput { sender: tx }, rx)
    }
}

}
