//! Output that generates and sends MIDI events.

use crate::{Command, Output, Play};
use crate::commands;
use crate::units::Pitch;

use anyhow::{Context, anyhow, bail};
use log::*;
use midir;
use rosc::OscType;
use tokio::sync::mpsc;

use std::collections::HashMap;
use std::str::FromStr;

pub const CONTROL_CHANGE_MSG: u8 = 0xB0;
pub const NOTE_ON_MSG: u8 = 0x90;
pub const NOTE_OFF_MSG: u8 = 0x80;
pub const PRESSURE_MSG: u8 = 0xA0;

pub struct MidiOutput {
    midi_out: midir::MidiOutputConnection,

    sender: mpsc::UnboundedSender<Command>,
    receiver: mpsc::UnboundedReceiver<Command>,
}

#[derive(Debug, PartialEq)]
pub enum MidiCommand {
    /// Set controller value.
    ControlChange { control: u8, value: u8 },
    /// Note on
    NoteOn { channel: u8, pitch: Pitch, velocity: u8 },
    /// Note off
    NoteOff { channel: u8, pitch: Pitch },
    /// Pressure / aftertouch.
    Pressure { pitch: Pitch, value: u8 },
}

pub fn parse_command(command: &Command) -> Result<MidiCommand, anyhow::Error> {
    if command.addr.starts_with("/cc/") {
        let control = command.addr[4..].parse::<u8>()
            .map_err(|_| anyhow!("Invalid MIDI CC property: {}", command.addr))?;
        let value: f64 = commands::expect_arg_f64(&command.args[0])?;
        let value_u8: u8 = (value * 127.0) as u8;
        return Ok(MidiCommand::ControlChange { control, value: value_u8 });
    };

    if command.addr.starts_with("/note_on") {
        let channel = commands::expect_arg_u8(&command.args[0])?;
        let pitch = commands::expect_arg_u8(&command.args[1])?;
        let velocity = commands::expect_arg_u8(&command.args[2])?;
        return Ok(MidiCommand::NoteOn { channel, pitch, velocity });
    }

    if command.addr.starts_with("/note_off") {
        let channel = commands::expect_arg_u8(&command.args[0])?;
        let pitch = commands::expect_arg_u8(&command.args[1])?;
        return Ok(MidiCommand::NoteOff { channel, pitch });
    }

    if command.addr.starts_with("/pressure") {
        let pitch = commands::expect_arg_u8(&command.args[0])?;
        let value = commands::expect_arg_u8(&command.args[1])?;
        return Ok(MidiCommand::Pressure { pitch, value });
    }

    Err(anyhow!("Unknown method: {}", command.addr))
}

impl Output for MidiOutput {
    fn get_play(self: &Self) -> Play {
        Play::new(self.sender.clone(), "midi")
    }

    fn process_commands(self: &mut Self) {
        let mut batch: Vec<Command> = Vec::new();

        while let Ok(next) = self.receiver.try_recv() {
            batch.push(next);
        }

        for command in batch.iter() {
            let parse_result = parse_command(command);
            if let Ok(command) = parse_result {
                debug!("MIDI command: {:?}", command);
                match command {
                    MidiCommand::ControlChange { control, value } =>
                        self.send(&[CONTROL_CHANGE_MSG, control, value]),
                    MidiCommand::NoteOn { channel, pitch, velocity } =>
                        self.send(&[NOTE_ON_MSG + channel, pitch as u8, velocity]),
                    MidiCommand::NoteOff { channel, pitch } =>
                        self.send(&[NOTE_OFF_MSG + channel, pitch as u8, 0]),
                    MidiCommand::Pressure { pitch, value } =>
                        self.send(&[PRESSURE_MSG, pitch as u8, value]),
                }
            } else {
                // FIXME: there should be validation when commands are posted so we can
                // detect these earlier.
                panic!("Ignoring invalid command: {:?}: {}", command, parse_result.err().unwrap());
            }
        }
    }
}

impl MidiOutput {
    pub fn new(midi_out: midir::MidiOutputConnection) -> MidiOutput {
        let (tx, rx) = mpsc::unbounded_channel();
        MidiOutput { midi_out, sender: tx, receiver: rx }
    }

    fn send(self: &mut Self, message: &[u8]) {
        info!("MIDI Message: {:?}", message);
        self.midi_out.send(message).unwrap();
    }
}

pub fn cc(cc: u8, value: u8) -> Command {
    Command { addr: format!("/midi/cc{cc}").to_string(), args: vec!(OscType::Int(i32::from(value))) }
}

pub fn note_on(channel: u8, pitch: Pitch, velocity: u8) -> Command {
    Command { addr: format!("/midi/note_on").to_string(), args: vec!(OscType::Int(i32::from(channel)), OscType::Int(i32::from(pitch)), OscType::Int(i32::from(velocity))) }
}

pub fn note_off(channel: u8, pitch: Pitch) -> Command {
    Command { addr: format!("/midi/note_off").to_string(), args: vec!(OscType::Int(i32::from(channel)), OscType::Int(i32::from(pitch))) }
}

pub fn pressure(pitch: Pitch, value: u8) -> Command {
    Command { addr: format!("/midi/pressure").to_string(), args: vec!(OscType::Int(i32::from(pitch)), OscType::Int(i32::from(value))) }
}

pub fn create_midi_output(mut config: HashMap<&str, &str>) -> Result<MidiOutput, anyhow::Error> {
    let mut port_index: usize = 0;

    for (key, value) in config.drain() {
        match key {
            "port" => {
                port_index = usize::from_str(value)
                    .with_context(|| format!("Couldn't parse `port` value: {}", value))?;
            }
            _ => {
                bail!("Unknown config key for midi output: {}", key)
            }
        }
    }

    let midi_out = midir::MidiOutput::new("Monome as Notes")?;

    let out_ports = midi_out.ports();
    let out_port = &out_ports[port_index];
    let out_port_name = midi_out.port_name(out_port)
        .map_err(|_| "No name")
        .unwrap();

    // The map_err() is a workaround for https://github.com/Boddlnagg/midir/issues/55
    let connection = midi_out.connect(out_port, "monome-as-notes")
        .map_err(|e| midir::ConnectError::new(e.kind(), ()))
        .with_context(|| format!("Couldn't connect to MIDI port {} ({})", port_index, out_port_name))?;

    Ok(MidiOutput::new(connection))
}

