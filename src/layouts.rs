use crate::Layout;

use std::collections::HashMap;

type LayoutCtor = fn() -> Layout;

pub fn layouts() -> HashMap<String, LayoutCtor> {
    let mut layouts = HashMap::<String, LayoutCtor>::new();
    layouts.insert(
        "line-keyboard.16".to_string(),
        || { linear_keyboard_layout::new(16, 0) }
    );
    layouts.insert(
        "line-keyboard.15".to_string(),
        || { linear_keyboard_layout::new(15, 0) }
    );
    layouts.insert(
        "line-keyboard.12".to_string(),
        || { linear_keyboard_layout::new(12, 12) }
    );
    layouts.insert(
        "line-keyboard.8".to_string(),
        || { linear_keyboard_layout::new(8, 36) }
    );
    layouts.insert(
        "line-keyboard.7".to_string(),
        || { linear_keyboard_layout::new(7, 36) }
    );
    layouts.insert(
        "line-keyboard.8.xy".to_string(),
        || { keyboard_xy_layout::new() }
    );
    //layouts.insert(
    //    "line-keyboard.8.sliders".to_string(),
    //    || { keyboard_sliders_layout::new() }
    //);
    layouts.insert(
        "pressure-keyboard.8.sliders".to_string(),
        || { pressure_keyboard_sliders_layout::new() }
    );
    return layouts;

}

pub mod keyboard_xy_layout {

const NAME: &str = "Keyboard XY layout";
const DESCRIPTION: &str = "A keyboard on the left and an XY pad on the right.";

use crate::{controls, Control, grid, Layout, Rect};

use std::sync::{Arc, Mutex};

pub fn new() -> Layout {
    let mut layout = Layout::new_empty(NAME, DESCRIPTION);

    let light_markings = true;

    let keyboard: Arc<Mutex<dyn Control>> = Arc::new(
        Mutex::new(
            controls::LinearKeyboard::new(36, light_markings, 0)
        )
    );
    let xy: Arc<Mutex<dyn Control>> = Arc::new(
        Mutex::new(
            controls::XYPad::new(20, 21)
        )
    );

    layout.add_control(
        "keyboard", Rect { x: 0, y: 0, w: 8, h: grid::MAX_Y, z: 0 }, Arc::clone(&keyboard),
    );
    layout.add_control(
        "xy", Rect { x: 8, y: 0, w: 8, h: grid::MAX_Y, z: 0 }, Arc::clone(&xy),
    );
    return layout;
}

}

pub mod linear_keyboard_layout {

const NAME: &str = "Linear keyboard layout";
const DESCRIPTION: &str = "The whole grid is made into a linear keyboard.";

use crate::{controls, Control, grid, Layout, Rect};
use crate::units::Pitch;

use std::sync::{Arc, Mutex};

pub fn new(keyboard_width: i32, lowest_pitch: Pitch) -> Layout {
    let mut layout = Layout::new_empty(NAME, DESCRIPTION);

    let light_markings = true;

    let keyboard: Arc<Mutex<dyn Control>> = Arc::new(
        Mutex::new(
            controls::LinearKeyboard::new(lowest_pitch, light_markings, 0)
        )
    );

    layout.add_control(
        "keyboard", Rect { x: 0, y: 0, w: keyboard_width, h: grid::MAX_Y, z: 0 }, Arc::clone(&keyboard),
    );
    return layout;
}

}

pub mod pressure_keyboard_sliders_layout {

const NAME: &str = "Pressure keyboard + sliders";
const DESCRIPTION: &str = "An 8-height pressure keyboard on the left and 8 vertical sliders on the right.";

use crate::{controls, Control, grid, Layout};
use crate::layout::{Orientation, Rect};

use std::sync::{Arc, Mutex};

pub fn new() -> Layout {
    let mut layout = Layout::new_empty(NAME, DESCRIPTION);

    let light_markings = true;

    let keyboard: Arc<Mutex<dyn Control>> = Arc::new(
        Mutex::new(
            controls::PressureKeyboard::new(36, light_markings, 0)
        )
    );
    layout.add_control(
        "keyboard", Rect { x: 0, y: 0, w: 8, h: grid::MAX_Y, z: 0 }, Arc::clone(&keyboard),
    );

    for i in 0..8 {
        let slider: Arc<Mutex<dyn Control>> = Arc::new(
            Mutex::new(
                controls::Slider::new(
                    Orientation::VERTICAL,
                    &format!("midi.cc{}", 20 + i),
                    0,
                    127,
                    127,
                )
            )
        );
        layout.add_control(
            "slider", Rect { x: 8 + i as i32, y: 0, w: 1, h: grid::MAX_Y, z: 0 }, Arc::clone(&slider),
        );
    }

    return layout;
}

}
