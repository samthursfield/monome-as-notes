//! Controls and other parts of the app communicate by sending commands around.
//!
//! The commands are regular OpenSoundControl messages.

use anyhow::{anyhow, bail};

use rosc::{OscMessage, OscType};

pub type Command = OscMessage;

/// Produce a Command from a string description.
///
/// Format is:
///
///   /method arg1 arg2 ...
///
/// All args are parsed as f64 types.
pub fn parse_command(method_with_args: &str) -> Result<Command, anyhow::Error> {
    let parts: Vec<&str> = method_with_args.split(' ').collect();
    let method = parts[0].to_string();
    let mut args: Vec<OscType> = Vec::new();
    if parts.len() > 1 {
        for arg in &parts[1..] {
            let value = arg.parse::<f64>()?;
            args.push(OscType::Double(value));
        }
    }
    Ok(Command { addr: method, args })
}

/// Split the first namespace component off a command.
///
/// Return a tuple with the first namespace component, and a new Command for the
/// rest of the address.
///
/// For example:
///
///     # use monome_as_notes::commands::{Command, split_namespace};
///     let command = Command { addr: "/foo/bar".to_string(), args: vec!() };
///     let (namespace, subcommand) = split_namespace(&command).unwrap();
///     assert_eq!(namespace, "foo");
///     assert_eq!(subcommand.addr, "/bar");
///
pub fn split_namespace(command: &Command) -> anyhow::Result<(&str, Command)> {
    if command.addr.len() > 0 && command.addr.get(0..1) == Some("/") {
        let method = &command.addr[1..];
        match method.match_indices('/').next() {
            Some((namespace_end, _)) => {
                let namespace = &method[0..namespace_end];
                let sub_command = OscMessage {
                    addr: method[namespace_end..].to_string(),
                    args: command.args.clone(),
                };
                Ok((namespace, sub_command))
            },
            None => bail!("Method path '{}' missing namespace.", command.addr),
        }
    } else {
        bail!("Method missing initial '/': {}", command.addr)
    }
}

/// Implemented by anything which can handle commands.
///
/// Usually not implemented by the struct itself, but a delegate which can have
/// multiple immutable references.
pub trait Commandable {
    fn post_command(self: &Self, command: Command) -> anyhow::Result<()>;
}

pub fn expect_arg_u8(arg: &OscType) -> Result<u8, anyhow::Error> {
    match arg {
        OscType::Long(value) => u8::try_from(*value).map_err(|e| anyhow!(e)),
        OscType::Int(value) => u8::try_from(*value).map_err(|e| anyhow!(e)),
        _ => bail!("Cannot convert {:?} to u8", arg),
    }
}

pub fn expect_arg_f64(arg: &OscType) -> Result<f64, anyhow::Error> {
    match arg {
        OscType::Double(value) => Ok(*value),
        _ => bail!("Cannot convert argument {:?} to f64", arg),
    }
}
