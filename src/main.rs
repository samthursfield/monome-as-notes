mod app_state;
mod commands;
mod controls;
mod draw;
mod event_loop;
mod grid;
mod layout;
mod layouts;
mod leds;
mod outputs;
mod play;
mod transport;
mod units;

pub use app_state::AppState;
pub use commands::{Command, Commandable};
pub use controls::Control;
pub use draw::{Draw, LedBrightness};
pub use grid::{MAX_X,MAX_Y};
pub use layout::{LayoutInfo, Layout};
pub use layout::Rect;
pub use leds::Leds;
pub use outputs::Output;
pub use play::Play;
pub use transport::Transport;

use anyhow::{Error, anyhow, bail};
use clap::{Arg, ArgAction};
use clap::builder::NonEmptyStringValueParser;
use log::*;
use monome::Monome;
use midir;

use std::collections::HashMap;
use std::env;

pub fn list_layouts() {
    for name in layouts::layouts().keys() {
        println!("{}", name);
    }
}

pub fn list_midi_ports() -> Result<(), Error> {
    let midi_out = midir::MidiOutput::new("Monome as Notes")?;

    println!("\nAvailable output ports:");
    for (i, p) in midi_out.ports().iter().enumerate() {
        println!(" • {}: {}", i, midi_out.port_name(p)?);
    }

    return Ok(())
}

pub fn list_outputs() {
    for name in outputs::all_outputs() {
        println!("{}", name);
    }
}

fn create_output(output_config: &str) -> Result<Box<dyn Output>, anyhow::Error> {
    let parts: Vec<&str> = output_config.split(',').collect();

    if parts.is_empty() {
        bail!("No output name specified");
    };

    let type_name = parts[0];
    let mut config: HashMap<&str, &str> = HashMap::new();
    for key_value in parts[1..].iter() {
        let (key, value) = key_value.split_once('=')
            .ok_or_else(|| anyhow!("Couldn't parse config setting: {}", key_value))?;
        config.insert(key, value);
    }

    outputs::create_output(&type_name, config)
}

async fn run_app() -> Result<(), Error> {
    let args = clap::Command::new("Monome as Notes")
        .version("1.0")
        .arg(Arg::new("layout")
             .long("layout")
             .short('l')
             .value_name("PATH")
             .default_value("line-keyboard.16")
             .help("Path to layout file or name of builtin layout to use."))
        .arg(Arg::new("output")
             .long("output")
             .short('o')
             .value_name("OUTPUT_CONFIG")
             .value_parser(NonEmptyStringValueParser::new())
             .help("Configure one output"))
        .arg(Arg::new("list-layouts")
             .long("list-layouts")
             .action(ArgAction::SetTrue)
             .help("Show available builtin layouts and exit."))
        .arg(Arg::new("list-midi-ports")
             .long("list-midi-ports")
             .action(ArgAction::SetTrue)
             .help("Show available MIDI ports and exit."))
        .arg(Arg::new("list-outputs")
             .long("list-outputs")
             .action(ArgAction::SetTrue)
             .help("Show available output types and exit."))
        .get_matches();

    if args.get_one("list-midi-ports") == Some(&true) {
        list_midi_ports()?;
        return Ok(());
    }

    if args.get_one("list-layouts") == Some(&true) {
        list_layouts();
        return Ok(());
    }

    if args.get_one("list-outputs") == Some(&true) {
        list_outputs();
        return Ok(());
    }

    let layout_path_or_name = args.get_one::<String>("layout").expect("has default");

    let layout = match layouts::layouts().get(layout_path_or_name) {
        Some(layout_ctor) => {
            info!("Using builtin layout {}", layout_path_or_name);
            layout_ctor()
        },
        None => {
            layout::load_layout_file(layout_path_or_name)
                .expect("Error loading layout file")
        }
    };

    let monome_option = match Monome::new("/prefix".to_string()) {
        Ok(monome) => Some(monome),
        Err(err) => {
            warn!("No grid device found: {:#}", err);
            None
        }
    };

    let output_info = args.get_one::<String>("output")
        .ok_or_else(|| anyhow!("At least one output must be defined with `--output`."))?;
    let output = create_output(&output_info)?;
    let transport = Transport::new();

    let leds = Leds::new();

    let state = AppState::new(
        layout.get_delegate(),
        transport.get_delegate(),
        leds.get_draw(),
        output.get_play(),
    );

    leds.get_draw().reset();
    event_loop::run(state, layout, transport, monome_option, leds, output).await;

    return Ok(());
}

#[tokio::main(flavor = "current_thread")]
async fn main() -> Result<(), anyhow::Error> {
    if let Err(_) = env::var("RUST_LOG") {
        env::set_var("RUST_LOG", "warn");
    };

    env_logger::init();

    run_app().await
}
