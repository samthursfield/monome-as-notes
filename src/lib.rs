//! Monome as Notes is a toolkit for using a [monome grid](https://monome.org/docs/grid/)
//! as a controller for musical instruments.
//!
//! The interface is defined by arranging one or more [controls](Control)
//! to create a [layout](Layout). This is usually done in a YAML file, the
//! format is documented in the [layout] module.
//!
//! ## Running monome-as-notes
//!
//! You run Monome as Notes from the commandline, as in the following example:
//!
//! ```bash
//! monome-as-notes --layout layouts/mylayout.yaml --output midi
//! ```
//!
//! It expects to use [serialosc](https://github.com/monome/serialosc) to be running
//! to talk to the grid device.
//!
//! There is an alternative grid backend that commicates over Open Sound Control to a small
//! Norns script to enable running on [Norns](https://monome.org/docs/norns/). This is enabled by
//! the `--grid-backend=norns` option.
//!
//! ## Choosing outputs
//!
//! Choose what device is controlled with the `--output` option. This takes an output name,
//! followed optionally by key=value configuration pairs.
//!
//! Use `--list-outputs` to see what output types are available.
//!
//! ### MIDI outputs
//!
//! The `midi` output goes to a specific MIDI port (default 0). You may need to configure this so it is
//! controlling the instrument you expect. You can see a list of MIDI ports by running the
//! following:
//!
//! ```bash
//! monome-as-notes --list-midi-ports
//! ```
//!
//! Use the `port=` config option to specify a port, for example:
//!
//! ```bash
//! monome-as-notes --output midi,port=2
//! ```
//!
//! ### Bitwig OSC output
//!
//! The Bitwig OSC output can control Bitwig Studio over the OpenSoundControl protocol.
//! It requires a 3rd party add-on named "DrivenByMoss".
//!
//! You can configure the OSC port with the `port=` option, the default is 9000.

pub mod app_state;
pub mod commands;
pub mod controls;
pub mod draw;
pub mod event_loop;
pub mod grid;
pub mod layout;
pub mod layouts;
pub mod leds;
pub mod outputs;
pub mod play;
pub mod transport;
pub mod units;

pub use app_state::AppState;
pub use commands::{Command, Commandable};
pub use controls::Control;
pub use draw::Draw;
pub use grid::{MAX_X,MAX_Y};
pub use layout::{LayoutInfo, Layout};
pub use layout::Rect;
pub use leds::Leds;
pub use outputs::Output;
pub use play::Play;
pub use transport::{Transport, TransportState};
