extern crate monome;

use crate::{AppState, Layout, Leds, Transport};
use crate::layout::{Point};
use crate::Output;

pub use monome::{KeyDirection, Monome, MonomeEvent};
pub use monome::MonomeEvent::*;

use tokio::time::{self, Duration};

pub fn handle_monome_event(state: &mut AppState, layout: &Layout, event: &MonomeEvent) {
    match event {
        MonomeEvent::GridKey { x, y, direction } => {
            let controls = layout.pick(*x, *y);
            for (candidate, rect) in controls {
                let touch = Point { x: *x, y: *y };
                let handled = candidate.lock().unwrap().handle_key(
                    state, rect, touch, direction
                );
                if handled {
                    break;
                }
            }
        },
        _ => {},
    }
}

pub async fn run(mut state: AppState, mut layout: Layout, mut transport: Transport,
                 mut monome_option: Option<Monome>, mut leds: Leds, mut output: Box<dyn Output>) {
    for (control, rect) in layout.pick_all() {
        control.lock().unwrap().show(&mut state, rect);
    }

    async move {
        let mut monome_poll_interval = time::interval(Duration::from_millis(1));

        loop {
            match monome_option  {
                Some(ref mut monome) => {
                    let poll_result = monome.poll();
                    if let Some(event) = poll_result {
                        handle_monome_event(&mut state, &layout, &event)
                    }
                },
                None => {},
            }

            layout.process_commands(state.get_draw());
            leds.process_commands(&mut monome_option);
            transport.process_commands();
            output.process_commands();

            monome_poll_interval.tick().await;
        }
    }.await;
}
