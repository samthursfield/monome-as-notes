//! Global app state in a single struct.

use crate::commands::{self, Command, Commandable};
use crate::draw::Draw;
use crate::layout::LayoutDelegate;
use crate::play::Play;
use crate::transport::TransportDelegate;

use anyhow::{anyhow, Result};

pub struct AppState {
    pub layout_delegate: LayoutDelegate,

    // Global tranport,
    pub transport_delegate: TransportDelegate,

    // Helper for batching grid LED updates.
    pub draw: Draw,

    // Helper for outputting MIDI.
    pub play: Play,
}

impl AppState {
    pub fn new(layout_delegate: LayoutDelegate,
               transport_delegate: TransportDelegate,
               draw: Draw,
               play: Play,
            ) -> AppState {
        AppState {
            layout_delegate,
            transport_delegate,
            draw,
            play,
        }
    }

    pub fn get_layout_delegate(self: &Self) -> &LayoutDelegate {
        &self.layout_delegate
    }

    pub fn get_transport_delegate(self: &Self) -> &TransportDelegate {
        &self.transport_delegate
    }

    pub fn get_draw(self: &Self) -> &Draw {
        &self.draw
    }

    pub fn get_play(self: &Self) -> &Play {
        &self.play
    }

    pub fn post_command(self: &mut Self, command: Command) -> Result<(), anyhow::Error> {
        let (namespace, sub_command) = commands::split_namespace(&command)?;

        match namespace {
            "layout" => self.layout_delegate.post_command(sub_command),
            "output" => self.play.post_command(sub_command),
            "transport" => self.transport_delegate.post_command(sub_command),
            _ => Err(anyhow!("Unknown namespace {} in: {}", namespace, command.addr)),
        }
    }
}

