mod testutils;

use monome_as_notes::Command;
use monome_as_notes::commands;

use rosc::OscType;

const LAYOUT: &str = "
---
name: Commands test
controls:
  - type: slider.vertical
    name: param-1
    pos: [8, 0]
    size: [1, 8]
    attrs:
      target: /midi/cc/20/set
";

#[test]
fn parse_method_call() {
    let command = commands::parse_command("/foo/bar")
        .unwrap();
    assert_eq!(command.addr, "/foo/bar");
    assert_eq!(command.args.len(), 0);

    let command = commands::parse_command("/foo/bar 54 46")
        .unwrap();
    assert_eq!(command.addr, "/foo/bar");
    assert_eq!(command.args, [OscType::Double(54.0), OscType::Double(46.0)]);

    let result = commands::parse_command("/foo/bar not-a-float");
    assert!(result.is_err());

    let result = commands::parse_command("/foo/bar     ");
    assert!(result.is_err());
}

#[test]
fn send_invalid_methods() {
    let mut app = testutils::TestApp::new_with_layout_yaml(LAYOUT);

    // Make sure nothing panics here.
    let result = app.state.post_command(
        Command { addr: "No namespace here".to_string(), args: Vec::new() }
    );
    assert!(result.is_err());

    let result = app.state.post_command(
        Command { addr: "/this is not a namespace/invalid".to_string(), args: Vec::new() }
    );
    assert!(result.is_err());
}

/*
#[test]
fn set_property_invalid_values() {
    let mut app = build_test_app();

    // Make sure nothing panics here.
    let result = app.set_property("midi.cc1", "banana");
    assert!(result.is_err());

    let result = app.set_property("transport.tempo", "banana");
    assert!(result.is_err());
}
*/
