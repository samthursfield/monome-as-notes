mod testutils;

use monome_as_notes::Command;
use monome_as_notes::layouts::linear_keyboard_layout;

use rosc::OscType;

#[test]
fn transport() {
    env_logger::init();

    let layout = linear_keyboard_layout::new(16, 36);
    let mut app = testutils::TestApp::new_with_layout(layout);

    // Returns the default tempo.
    assert_eq!(app.transport.get_tempo(), 60.0);

    app.state.post_command(
        Command {
            addr: "/transport/tempo".to_string(),
            args: vec!(OscType::Double(9999.5))
        }).unwrap();

    app.transport.process_commands();

    // New tempo is set.
    assert_eq!(app.transport.get_tempo(), 9999.5);
}
