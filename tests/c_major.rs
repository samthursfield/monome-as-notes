mod testutils;

use monome_as_notes::AppState;
use monome_as_notes::event_loop;
use monome_as_notes::layouts::linear_keyboard_layout;
use monome_as_notes::Layout;
use monome_as_notes::outputs::midi::{self, MidiCommand};

mod mock_events {
    use super::AppState;
    use super::Layout;
    use crate::event_loop::{handle_monome_event, KeyDirection, MonomeEvent};

    pub fn grid_key_down(state: &mut AppState, layout: &Layout, x: i32, y: i32) {
        let event = MonomeEvent::GridKey {
            x, y, direction: KeyDirection::Down
        };

        handle_monome_event(state, layout, &event);
    }

    pub fn grid_key_up(state: &mut AppState, layout: &Layout, x: i32, y: i32) {
        let event = MonomeEvent::GridKey {
            x, y, direction: KeyDirection::Up
        };

        handle_monome_event(state, layout, &event);
    }
}

#[test]
fn c_major() {
    env_logger::init();

    const PITCH_C3: u8 = 36;
    const PITCH_D3: u8 = 38;
    const PITCH_E3: u8 = 40;

    let layout = linear_keyboard_layout::new(16, PITCH_C3);
    let mut app = testutils::TestApp::new_with_layout(layout);

    mock_events::grid_key_down(&mut app.state, &app.layout, 0, 7);
    mock_events::grid_key_up(&mut app.state, &app.layout, 0, 7);
    mock_events::grid_key_down(&mut app.state, &app.layout, 2, 7);
    mock_events::grid_key_up(&mut app.state, &app.layout, 2, 7);
    mock_events::grid_key_down(&mut app.state, &app.layout, 4, 7);
    mock_events::grid_key_up(&mut app.state, &app.layout, 4, 7);
    app.output_rx.close();

    let mut commands = Vec::<MidiCommand>::new();
    while let Some(msg) = app.output_rx.blocking_recv() {
        println!("Got: {:?}",msg);
        let midi_command = midi::parse_command(&msg).unwrap();
        commands.push(midi_command);
    }

    let velocity: u8 = 0x64;
    assert_eq!(commands.len(), 6);
    assert_eq!(commands[0], MidiCommand::NoteOn { channel: 0, pitch: PITCH_C3, velocity });
    assert_eq!(commands[1], MidiCommand::NoteOff { channel: 0, pitch: PITCH_C3 });
    assert_eq!(commands[2], MidiCommand::NoteOn { channel: 0, pitch: PITCH_D3, velocity });
    assert_eq!(commands[3], MidiCommand::NoteOff { channel: 0, pitch: PITCH_D3 });
    assert_eq!(commands[4], MidiCommand::NoteOn { channel: 0, pitch: PITCH_E3, velocity });
    assert_eq!(commands[5], MidiCommand::NoteOff { channel: 0, pitch: PITCH_E3 });
}
