use assert_cmd::prelude::*;

use std::process::Command;

#[test]
fn outputs() -> Result<(), Box<dyn std::error::Error>> {
    let mut cmd = Command::cargo_bin("monome-as-notes")?;
    cmd.args(["--list-outputs"]);
    cmd.assert().success();

    let mut cmd = Command::cargo_bin("monome-as-notes")?;
    cmd.args(["--output=bad"]);
    cmd.assert().failure();

    let mut cmd = Command::cargo_bin("monome-as-notes")?;
    cmd.args(["--output=midi,invalidportconfig"]);
    cmd.assert().failure();

    Ok(())
}
