mod testutils;

use monome_as_notes::*;

mod mock_events {
    use super::AppState;
    use super::Layout;
    use crate::event_loop::{handle_monome_event, KeyDirection, MonomeEvent};

    pub fn grid_key_down(state: &mut AppState, layout: &Layout, x: i32, y: i32) {
        let event = MonomeEvent::GridKey {
            x, y, direction: KeyDirection::Down
        };

        handle_monome_event(state, layout, &event);
    }

    pub fn grid_key_up(state: &mut AppState, layout: &Layout, x: i32, y: i32) {
        let event = MonomeEvent::GridKey {
            x, y, direction: KeyDirection::Up
        };

        handle_monome_event(state, layout, &event);
    }
}

#[test]
/// Test two sliders which control each other's value.
///
/// Among other things, this tests that no command-loop cycles can occur.
fn two_sliders() {
    env_logger::init();

    let layout_path = testutils::test_data(&"tests/layout/2sliders.yaml");
    let layout = layout::load_layout_file(&layout_path)
        .unwrap();
    let mut app = testutils::TestApp::new_with_layout(layout);

    mock_events::grid_key_down(&mut app.state, &app.layout, 0, 7);
    mock_events::grid_key_up(&mut app.state, &app.layout, 0, 7);

    app.layout.process_commands(&app.leds.get_draw());
}
