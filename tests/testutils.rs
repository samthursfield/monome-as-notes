#![allow(dead_code)]

use monome_as_notes::AppState;
use monome_as_notes::Command;
use monome_as_notes::{Layout, LayoutInfo};
use monome_as_notes::Leds;
use monome_as_notes::outputs::{Output, TestOutput};
use monome_as_notes::Transport;

use tokio::sync::mpsc;

pub struct TestApp {
    pub state: AppState,
    pub layout: Layout,
    pub transport: Transport,
    pub output_rx: mpsc::UnboundedReceiver<Command>,
    pub leds: Leds,
}

impl TestApp {
    pub fn new_with_layout(layout: Layout) -> TestApp {
        let transport = Transport::new();
        let (output, output_rx) = TestOutput::new();
        let leds = Leds::new();

        let state = AppState::new(
            layout.get_delegate(),
            transport.get_delegate(),
            leds.get_draw(),
            output.get_play(),
        );

        TestApp {
            state,
            layout,
            transport,
            output_rx,
            leds
        }
    }

    pub fn new_with_layout_yaml(layout_yaml: &str) -> TestApp {
        let layout_info: LayoutInfo = serde_yaml::from_str(layout_yaml)
            .unwrap();
        let layout = Layout::new_from_info(&layout_info);
        return Self::new_with_layout(layout);
    }
}

pub fn test_data(subpath: &str) -> String {
    // This works for now, but may need extending to use std::env::current_exe.
    // See: https://users.rust-lang.org/t/how-to-reliably-locate-test-data-files-from-tests/3507/2
    subpath.to_string()
}
