mod testutils;

const LAYOUT: &str = "
---
name: Slider test
description: This is required for some reason.
controls:
  - type: slider.vertical
    name: param-1
    pos: [8, 0]
    size: [1, 8]
    attrs:
      target: midi.cc20
";

#[test]
fn slider_basic() {
    env_logger::init();

    let _app = testutils::TestApp::new_with_layout_yaml(LAYOUT);
}
