# Monome as Notes

Work in progress: tool for mapping [Monome grid](https://monome.org/docs/grid/)
to musical notes.

For more info see [the reference manual](https://samthursfield.gitlab.io/monome-as-notes/monome_as_notes/).

## Related projects and threads

 * [Control](https://llllllll.co/t/control-open-midi-controller-platform-for-128-grids/2128), a patch for Max/MSP
 * [Monome Notes](https://github.com/monome-community/monome-notes), an app only for Mac OS X
 * Lines forum - [How to use grid as dumb, 128 note keyboard?](https://llllllll.co/t/how-to-use-grid-as-dumb-128-note-keyboard/5205/2)
 
## Other discussions

  * https://github.com/p3r7/gridkeys
  * https://www.elektronauts.com/t/grid-based-midi-keyboard/146185
