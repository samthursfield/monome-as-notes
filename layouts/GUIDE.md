# Guide to the layouts

The layouts are intended to be useful on multiple devices.

In case you're interested in layouts for a specific piece of software
or device, this document gathers recommendations.

## Bitwig Studio (DAW)

Bitwig Studio groups parameters into sets of 8.

Useful layouts:

  * keyboard-sliders-8.yaml

## Electron Model:Cycles (Groovebox)

Groovebox which receives MIDI on channels 1-6.

Useful layouts:

  * groovebox-4+2.yaml
