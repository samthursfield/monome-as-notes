So many ideas, so little time.

Keyboard:

  * keyline: 1 parameter eg. pitch, in a line, wrapping
  * strings: like a bass guitar
  * keybox: 1 x and 1 y parameter (e.g. pitch, velocity)
      * How do you specify the x and y axis?
          attrs:
            x: midi-cc(cc=22,min=0,max=100)
            y: pitch(min=36)
        or
          attrs:
            x: velocity
            y: 
            pitch: 
        or like:
          config:
            x:
              controls: midi-cc.22
              min: 0
              max: 100
            y:
              controls: midi-cc.23
              min: 0
              max: 100
            pitch: 36
            velocity: 67

       ^^ can serde read nested dicts like this where the value may be a stirng or another dict??

  * a "utility bar" above the keyboard that can:
      - shift 8ve up or down
      - toggle 'sticky notes'
      - page flip to 8 sliders ?
      - pitch bend
          


Testcases

  * All just about testing that button press / gesture causes a certain effect...

More types of control:

 * Binary select: use 8 buttons to select 0-255, or 16 buttons to select 0-65535
 * Press 2 buttons - select value in between?
 * Long press a button - vibrato ? Press neighbours for wider
 * Press current position, then another buttons - slide between points (maybe the time taken to press 2nd button controls the slide time)
 * Double press - 
 * Keyboard: gestures as with the slider

Beat repeats:

  * Add a BPM setting and a beat-repeat paramter
  * Beat pattern - could use a "binary select" control to define various patterns...

GUI:

  * Start by adding support for drawing layouts as .png files using Cairo - could be a separate binary
  * If/when this looks like, wrap in a GTK application
  * Keep a non GTK/Cairo option for portability
